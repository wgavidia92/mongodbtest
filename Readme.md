#MongoDBTest

### 1. Overview

This project follows the *[Start with Guides](https://www.mongodb.com/docs/guides/)* from **[mongodb.com](https://www.mongodb.com/)** to recreate a connection with a NoSQL database.

### 2. Requirements

To run this project in your local you will need:

* Java 19+


* MongoDB account - Follow the guide [Sign Up for a MongoDB Account](https://www.mongodb.com/docs/guides/atlas/account/)
* MongoDB cluster - Follow the guide [Create a Cluster](https://www.mongodb.com/docs/guides/atlas/cluster/)
* MongoDB user and password - Follow the guide [Add a Database User](https://www.mongodb.com/docs/guides/atlas/db-user/)

### 3. Run the application

To run the application you should follow this steps:

**1** Clone or download this project

**2** Open the project in your favorite IDE, (Eclipse, IntelliJ, etc)

**3** Update the **application.properties** file in the path *mongodbtest/app/src/main/resources* adding your credentials and changing the url with yours.

>
> **NOTE:** the url property must have the words {username} and {password} with the brackets. This information is replaced with the values provided in the username and password properties when the 
connection is created by java
>
> ```properties
> db.username=
> db.password=
> db.url=mongodb+srv://{username}:{password}@wilmertestdb.rlnteys.mongodb.net/?retryWrites=true&w=majority
> db.name=sample_guides
> ```
>

**4** Run the main method in the class **App.java**, located in the src folder *mongodbtest/app/src/main/java/com/mongodbtest*

### 4. Architecture

Although the code and the guide is very simple to follow, this project was implemented following a structure that allows a basic Dependency Injection. To understand better this, The following packages were created:

1. **Repository:** Classes and interfaces to get data from MongoDB.
2. **Service:** Classes and interfaces to keep the business logic of the application. (Not business logic required for this. The classes are bridges between the controller and the repositories).
3. **Controller:** Single class to simulate an end-point where all the methods are exposed to an external application.
4. **dto:** Java ojbects to transfer information between the layers.
5. **infrastructure:** Factories that create the instances and act provides the dependencies required for each instance.
	* **dbconnection:** Class that creates the connection with the database and returns the Collections required.
	* **repository:** Factory to create the instance of the repositories injecting a MongoDB collection as a dataSource, (Uses *Connection.java* class).
	* **service:** Factory to create the instance of the services injecting the repositories required for each instance, (Uses *RepositoryFactory.java* class).
	* **ApplicationFactory.java:** Factory to create the instance of the controller injecting the services, (Uses *ServiceFactory.java* class).
6. **App.java:** Main class where the *ApplicationFactory* class is called to create the instance of the controller and call all the methods defined.

![Components](./components.jpg)