package com.mongodbtest.repository;

import java.util.List;
import java.util.Map;

import org.bson.BsonObjectId;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.InsertManyResult;
import com.mongodb.client.result.UpdateResult;
import com.mongodbtest.dto.Comet;

class CometRepositoryTest {

	@Mock
	private MongoCollection<Document> dataSource;
	@Mock
	private InsertManyResult results;
	@Mock
	private UpdateResult updateResults;
	@Mock
	private DeleteResult deleteResults;

	private CometRepository repository;

	@BeforeEach
	void init() {
		MockitoAnnotations.openMocks(this);
		repository = new CometRepositoryImplementation(dataSource);
	}

	@Test
	void insertCometsTest() {
		var comets = List.of(new Comet("comet", "comet test", 1, 1, 1));

		Mockito.when(dataSource.insertMany(Mockito.anyList()))
			.thenReturn(results);
		Mockito.when(results.getInsertedIds())
			.thenReturn(Map.of(1, new BsonObjectId(ObjectId.get())));

		var ids = repository.insertComets(comets);
		Assertions.assertNotNull(ids);
		Assertions.assertFalse(ids.isEmpty());
		Assertions.assertNotNull(ids.get(0));
	}

	@Test
	void updateRadiusTest() {
		Mockito.when(dataSource.updateMany(Mockito.any(Bson.class), Mockito.any(Bson.class)))
			.thenReturn(updateResults);
		Mockito.when(updateResults.getModifiedCount())
			.thenReturn(1l);

		var radius = 1.2;
		var updatedRecords = repository.updateRadius(radius);

		Assertions.assertEquals(1l, updatedRecords);
	}

	@Test
	void deleteWithOrbiltalPeriodBetweenTest() {
		Mockito.when(dataSource.deleteMany(Mockito.any(Bson.class)))
			.thenReturn(deleteResults);
		Mockito.when(deleteResults.getDeletedCount())
			.thenReturn(1L);

		var lowerOrbitalPeriod = 5;
		var higherOrbitalPeriod = 85;

		var deletedRecords = repository.deleteWithOrbiltalPeriodBetween(lowerOrbitalPeriod, higherOrbitalPeriod);
		Assertions.assertEquals(1L, deletedRecords);
	}

	@Test
	void deleteAllTest() {
		Mockito.when(dataSource.deleteMany(Mockito.any(Bson.class)))
			.thenReturn(deleteResults);
		Mockito.when(deleteResults.getDeletedCount())
			.thenReturn(2L);

		var deletedRecords = repository.deleteAll();
		Assertions.assertEquals(2L, deletedRecords);
	}

}
