package com.mongodbtest.repository;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

class PlanetRepositoryTest {

	@Mock
	private MongoCollection<Document> dataSource;
	@Mock
	private FindIterable<Document> resultSet;
	@Mock
	private MongoCursor<Document> cursor;

	private PlanetRepository repository;

	@BeforeEach
	void init() {
		MockitoAnnotations.openMocks(this);
		repository = new PlanetRepositoryImplementation(dataSource);

		Mockito.when(dataSource.find(Mockito.any(Bson.class)))
			.thenReturn(resultSet);
		Mockito.when(resultSet.iterator())
			.thenReturn(cursor);
		Mockito.when(cursor.hasNext())
			.thenReturn(true)
			.thenReturn(false);
		Mockito.when(cursor.next())
			.thenReturn(new Document());
	}

	@Test
	void findAllPlanetsTest() {
		var result = repository.findAllPlanets();
		Assertions.assertNotNull(result);
		Assertions.assertEquals(1, result.size());
	}

	@Test
	void findPlanetsWithRingsTest() {
		var result = repository.findPlanetsWithRings();
		Assertions.assertNotNull(result);
		Assertions.assertEquals(1, result.size());
	}

}
