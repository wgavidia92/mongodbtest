package com.mongodbtest.service;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.mongodbtest.dto.Comet;
import com.mongodbtest.repository.CometRepository;

class CometServiceTest {

	@Mock
	private CometRepository repository;

	private CometService service;

	@BeforeEach
	void init() {
		MockitoAnnotations.openMocks(this);
		service = new CometServiceImplementation(repository);
	}

	@Test
	void addCometsTest() {
		Mockito.when(repository.insertComets(Mockito.anyList()))
			.thenReturn(List.of("1"));

		var ids = service.addComets(List.of(new Comet("comet", "comet", 1, 1, 1)));
		Assertions.assertNotNull(ids);
		Assertions.assertFalse(ids.isEmpty());
		Assertions.assertEquals(1, ids.size());
	}

	@Test
	void changeRadiusTest() {
		Mockito.when(repository.updateRadius(Mockito.anyDouble()))
			.thenReturn(1l);

		var radius = 1.2;
		var updatedRecords = service.changeRadius(radius);

		Assertions.assertEquals(1l, updatedRecords);
	}

	@Test
	void deleteCometsWithOrbitalPeriodBetweenTest() {
		Mockito.when(repository.deleteWithOrbiltalPeriodBetween(Mockito.anyDouble(), Mockito.anyDouble()))
			.thenReturn(1L);

		var lowerOrbitalPeriod = 5;
		var higherOrbitlaPeriod = 85;

		var deletedRecords = service.deleteCometsWithOrbitalPeriodBetween(lowerOrbitalPeriod, higherOrbitlaPeriod);
		Assertions.assertEquals(1L, deletedRecords);
	}

	@Test
	void deleteAllCometsTest() {
		Mockito.when(repository.deleteAll())
			.thenReturn(1L);

		var deletedRecords = service.deleteAllComets();
		Assertions.assertEquals(1L, deletedRecords);
	}

}
