package com.mongodbtest.service;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.mongodbtest.repository.PlanetRepository;

class PlanetServiceTest {

	@Mock
	private PlanetRepository repository;

	private PlanetService service;

	@BeforeEach
	void init() {
		MockitoAnnotations.openMocks(this);
		service = new PlanetServiceImplementation(repository);
	}

	@Test
	void getAllPlanetsTest() {
		Mockito.when(repository.findAllPlanets())
			.thenReturn(List.of("{name:earth}"));

		var planets = service.getAllPlanets();
		Assertions.assertNotNull(planets);
		Assertions.assertFalse(planets.isEmpty());
		Assertions.assertEquals(1, planets.size());
	}

	@Test
	void getPlanetsWithRingsTest() {
		Mockito.when(repository.findPlanetsWithRings())
			.thenReturn(List.of("{name:saturn}"));

		var planets = service.getPlanetsWithRings();
		Assertions.assertNotNull(planets);
		Assertions.assertFalse(planets.isEmpty());
		Assertions.assertEquals(1, planets.size());
	}

}
