package com.mongodbtest.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.mongodbtest.service.CometService;
import com.mongodbtest.service.PlanetService;

class PlanetsControllerTest {

	@Mock
	private Logger log;
	@Mock
	private PlanetService palentService;
	@Mock
	private CometService cometService;

	private PlanetsController controller;

	@BeforeEach
	void init() {
		MockitoAnnotations.openMocks(this);
		controller = new PlanetsController(log, palentService, cometService);
	}

	@Test
	void getAllPlanetsTest() {
		Mockito.when(palentService.getAllPlanets())
			.thenReturn(List.of("{Planet1}"));
		Assertions.assertDoesNotThrow(() -> controller.getAllPlanets());
	}

	@Test
	void getPlanetsWithRingsTest() {
		Mockito.when(palentService.getPlanetsWithRings())
			.thenReturn(List.of("{Planet2}"));
		Assertions.assertDoesNotThrow(() -> controller.getPlanetsWithRings());
	}

	@Test
	void putCometsTest() {
		Mockito.when(cometService.addComets(Mockito.anyList()))
			.thenReturn(List.of("1", "2", "3"));
		Assertions.assertDoesNotThrow(() -> controller.putComets());
	}

	@Test
	void changeCometRadiusTest() {
		Mockito.when(cometService.changeRadius(Mockito.anyDouble()))
			.thenReturn(2L);
		Assertions.assertDoesNotThrow(() -> controller.changeCometRadius());
	}

	@Test
	void deleteCometsWithOrbitalPeriodBetweenTest() {
		Mockito.when(cometService.deleteCometsWithOrbitalPeriodBetween(Mockito.anyDouble(), Mockito.anyDouble()))
			.thenReturn(1L);
		Assertions.assertDoesNotThrow(() -> controller.deleteCometsWithOrbitalPeriodBetween());
	}

	@Test
	void deleteAllCometsTest() {
		Mockito.when(cometService.deleteAllComets())
			.thenReturn(1L);
		Assertions.assertDoesNotThrow(() -> controller.deleteAllComets());
	}

}
