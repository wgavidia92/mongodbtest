package com.mongodbtest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.mongodbtest.controller.PlanetsController;
import com.mongodbtest.infrastructure.ApplicationFactory;

class AppTest {

	@Mock
	private PlanetsController controller;

	@BeforeEach
	void init() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	void mainTest() {
		try (var factory = Mockito.mockStatic(ApplicationFactory.class)) {
			factory.when(() -> ApplicationFactory.controller())
				.thenReturn(controller);
			
			Assertions.assertDoesNotThrow(() -> new App());
			Assertions.assertDoesNotThrow(() -> App.main(null));
		}
	}

}
