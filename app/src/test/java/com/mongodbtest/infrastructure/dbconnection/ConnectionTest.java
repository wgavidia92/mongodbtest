package com.mongodbtest.infrastructure.dbconnection;

import org.bson.Document;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

class ConnectionTest {

	@Mock
	private MongoClient client;
	@Mock
	private MongoDatabase database;
	@Mock
	private MongoCollection<Document> collection;

	@BeforeEach
	void init() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	void connectTest() {
		try (var mongoClient = Mockito.mockStatic(MongoClients.class)) {
			mongoClient.when(() -> MongoClients.create(Mockito.anyString()))
				.thenReturn(client);
			Mockito.when(client.getDatabase(Mockito.anyString()))
				.thenReturn(database);
			Mockito.when(database.getCollection(Mockito.anyString()))
				.thenReturn(collection);

			var db = Connection.connect(Connection.PLANETS_DOCUMENT);
			var db2 = Connection.connect(Connection.COMETS_DOCUMENT);
			Assertions.assertNotNull(db);
			Assertions.assertNotNull(db2);
		}
	}

	@Test
	void disconnectTest() {
		Assertions.assertDoesNotThrow(() -> Connection.disconect());
	}

}
