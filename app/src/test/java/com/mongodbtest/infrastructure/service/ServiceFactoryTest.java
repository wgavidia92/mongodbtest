package com.mongodbtest.infrastructure.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.mongodbtest.infrastructure.repository.RepositoryFactory;

class ServiceFactoryTest {

	@Test
	void planetServiceTest() {
		try (var factory = Mockito.mockStatic(RepositoryFactory.class)) {
			factory.when(RepositoryFactory::createPlanetRepository)
				.thenReturn(null);

			var service = ServiceFactory.planetService();
			Assertions.assertNotNull(service);
		}
	}

	@Test
	void cometServiceTest() {
		try (var factory = Mockito.mockStatic(RepositoryFactory.class)) {
			factory.when(RepositoryFactory::createCometRepository)
				.thenReturn(null);

			var service = ServiceFactory.cometService();
			Assertions.assertNotNull(service);
		}
	}

}
