package com.mongodbtest.infrastructure;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.mongodbtest.infrastructure.service.ServiceFactory;

class ApplicationFactoryTest {

	@Test
	void createLoggerTest() {
		try (var factory = Mockito.mockStatic(ServiceFactory.class)) {
			factory.when(ServiceFactory::planetService)
				.thenReturn(null);
			factory.when(ServiceFactory::cometService)
				.thenReturn(null);

			var controller = ApplicationFactory.controller();
			Assertions.assertNotNull(controller);
		}
	}

	@Test
	void endAppTest() {
		Assertions.assertDoesNotThrow(() -> ApplicationFactory.endApp());
	}

}
