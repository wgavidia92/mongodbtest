package com.mongodbtest.infrastructure.repository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.mongodbtest.infrastructure.dbconnection.Connection;

class RepositoryFactoryTest {

	@Test
	void createPlanetRepositoryTest() {
		Assertions.assertTrue(true);
		try (var connection = Mockito.mockStatic(Connection.class)) {
			connection.when(() -> Connection.connect(Mockito.anyString()))
				.thenReturn(null);

			var planetRepository = RepositoryFactory.createPlanetRepository();
			Assertions.assertNotNull(planetRepository);
		}
	}

	@Test
	void createCometRepositoryTest() {
		Assertions.assertTrue(true);
		try (var connection = Mockito.mockStatic(Connection.class)) {
			connection.when(() -> Connection.connect(Mockito.anyString()))
				.thenReturn(null);

			var cometRepository = RepositoryFactory.createCometRepository();
			Assertions.assertNotNull(cometRepository);
		}
	}

	@Test
	void disconect() {
		Assertions.assertDoesNotThrow(() -> RepositoryFactory.closeConection());
	}

}
