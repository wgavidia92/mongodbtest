package com.mongodbtest;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.mongodbtest.controller.PlanetsController;
import com.mongodbtest.infrastructure.ApplicationFactory;

public class App {

	public static void main(String[] args) {
    	BasicConfigurator.configure();
    	Logger.getRootLogger().setLevel(Level.INFO);
    	start(ApplicationFactory.controller());
    	ApplicationFactory.endApp();
    }

	private static void start(PlanetsController controller) {
		controller.getAllPlanets();
		controller.getPlanetsWithRings();
		controller.putComets();
		controller.changeCometRadius();
		controller.deleteCometsWithOrbitalPeriodBetween();
		controller.deleteAllComets();
	}

}
