package com.mongodbtest.controller;

import java.util.List;

import org.apache.log4j.Logger;

import com.mongodbtest.dto.Comet;
import com.mongodbtest.service.CometService;
import com.mongodbtest.service.PlanetService;

public class PlanetsController {

	private final Logger log;
	private final PlanetService palentService;
	private final CometService cometService;

	public PlanetsController(Logger log, PlanetService service, CometService cometService) {
		this.log = log;
		this.palentService = service;
		this.cometService = cometService;
	}

	public void getAllPlanets() {
		var planets = palentService.getAllPlanets();
		printList("==================== All planets ====================", planets);
	}

	public void getPlanetsWithRings() {
		var planets = palentService.getPlanetsWithRings();
		printList("================= Planets with rings ================", planets);
	}

	public void putComets() {
		var comets = List.of(
				new Comet("Halley's Comet", "1P/Halley", 75, 3.4175, 2.2e14),
				new Comet("Wild2", "81P/Wild", 6.41, 1.5534, 2.3e13),
				new Comet("Comet Hyakutake", "C/1996 B2", 17000, 0.77671, 8.8e12)
			);

		var insertedComets = cometService.addComets(comets);
		printList("================ IDs inserted comets ================", insertedComets);
	}

	public void changeCometRadius() {
		var radius = 1.60934;
		var updatedRecords = cometService.changeRadius(radius);
		printData("================== Updated commets ==================", updatedRecords);
	}

	public void deleteCometsWithOrbitalPeriodBetween() {
		var lowerOrbitalPeriod = 5;
		var higherOrbitalPeriod = 85;
		var deletedRecords = cometService.deleteCometsWithOrbitalPeriodBetween(lowerOrbitalPeriod, higherOrbitalPeriod);
		printData("================== Deleted commets ==================", deletedRecords);
	}

	public void deleteAllComets() {
		var deletedRecords = cometService.deleteAllComets();
		printData("================== Deleted commets ==================", deletedRecords);
	}

	private void printList(String message, List<String> data) {
		log.info(message);
		data.forEach(log::info);
	}

	private void printData(String message, Object data) {
		log.info(message);
		log.info(data);
	}

}
