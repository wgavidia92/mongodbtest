package com.mongodbtest.dto;

public record Comet (
			String name,
			String officialName,
			double orbitalPeriod,
			double radius,
			double mass
		) {
}
