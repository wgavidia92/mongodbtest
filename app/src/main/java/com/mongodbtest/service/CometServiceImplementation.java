package com.mongodbtest.service;

import java.util.List;

import com.mongodbtest.dto.Comet;
import com.mongodbtest.repository.CometRepository;

public final class CometServiceImplementation implements CometService {

	private final CometRepository repository;

	public CometServiceImplementation(CometRepository repository) {
		this.repository = repository;
	}

	@Override
	public List<String> addComets(List<Comet> comets) {
		return repository.insertComets(comets);
	}

	@Override
	public long changeRadius(double radius) {
		return repository.updateRadius(radius);
	}

	@Override
	public long deleteCometsWithOrbitalPeriodBetween(double lowerOrbitalPeriod, double higherOrbitalPeriod) {
		return repository.deleteWithOrbiltalPeriodBetween(lowerOrbitalPeriod, higherOrbitalPeriod);
	}

	@Override
	public long deleteAllComets() {
		return repository.deleteAll();
	}

}
