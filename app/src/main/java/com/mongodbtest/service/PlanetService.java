package com.mongodbtest.service;

import java.util.List;

public interface PlanetService {

	List<String> getAllPlanets();

	List<String> getPlanetsWithRings();

}
