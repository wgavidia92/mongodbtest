package com.mongodbtest.service;

import java.util.List;

import com.mongodbtest.repository.PlanetRepository;

public final class PlanetServiceImplementation implements PlanetService {

	private final PlanetRepository repository;

	public PlanetServiceImplementation(PlanetRepository repository) {
		this.repository = repository;
	}

	@Override
	public List<String> getAllPlanets() {
		return repository.findAllPlanets();
	}

	@Override
	public List<String> getPlanetsWithRings() {
		return repository.findPlanetsWithRings();
	}

}
