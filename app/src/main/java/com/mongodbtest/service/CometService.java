package com.mongodbtest.service;

import java.util.List;

import com.mongodbtest.dto.Comet;

public interface CometService {

	List<String> addComets(List<Comet> comets);

	long changeRadius(double radius);

	long deleteCometsWithOrbitalPeriodBetween(double lowerOrbitalPeriod, double higherOrbitalPeriod);

	long deleteAllComets();
}
