package com.mongodbtest.infrastructure.repository;

import com.mongodbtest.infrastructure.dbconnection.Connection;
import com.mongodbtest.repository.CometRepository;
import com.mongodbtest.repository.CometRepositoryImplementation;
import com.mongodbtest.repository.PlanetRepository;
import com.mongodbtest.repository.PlanetRepositoryImplementation;

public final class RepositoryFactory {

	private RepositoryFactory() {}

	public static PlanetRepository createPlanetRepository() {
		var dataSource = Connection.connect(Connection.PLANETS_DOCUMENT);
		return new PlanetRepositoryImplementation(dataSource);
	}

	public static CometRepository createCometRepository() {
		var dataSource = Connection.connect(Connection.COMETS_DOCUMENT);
		return new CometRepositoryImplementation(dataSource);
	}

	public static void closeConection() {
		Connection.disconect();
	}

}
