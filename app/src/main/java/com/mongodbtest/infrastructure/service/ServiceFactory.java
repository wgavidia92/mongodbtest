package com.mongodbtest.infrastructure.service;

import com.mongodbtest.infrastructure.repository.RepositoryFactory;
import com.mongodbtest.service.CometService;
import com.mongodbtest.service.CometServiceImplementation;
import com.mongodbtest.service.PlanetService;
import com.mongodbtest.service.PlanetServiceImplementation;

public class ServiceFactory {

	private ServiceFactory() {}

	public static PlanetService planetService() {
		var repository = RepositoryFactory.createPlanetRepository();
		return new PlanetServiceImplementation(repository);
	}

	public static CometService cometService() {
		var repository = RepositoryFactory.createCometRepository();
		return new CometServiceImplementation(repository);
	}

	public static void endApp() {
		RepositoryFactory.closeConection();
	}

}
