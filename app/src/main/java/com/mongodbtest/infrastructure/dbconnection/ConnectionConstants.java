package com.mongodbtest.infrastructure.dbconnection;

final class ConnectionConstants {

	private ConnectionConstants() {}

	public static final String DB_URL_PROP_KEY = "db.url";
	public static final String DB_PASSWORD_PROP_KEY = "db.username";
	public static final String DB_USERNAME_PROP_KEY = "db.password";
	public static final String DB_NAME_PROP_KEY = "db.name";

	public static final String USERNAME = "{username}";
	public static final String PASSWORD = "{password}";

}
