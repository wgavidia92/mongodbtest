package com.mongodbtest.infrastructure.dbconnection;

import java.io.IOException;
import java.util.Properties;

import org.bson.Document;
import org.apache.log4j.Logger;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodbtest.infrastructure.ApplicationFactory;

public final class Connection {

	private Connection() {}

	private static Properties properties = new Properties();
	private static Logger log = ApplicationFactory.createLogger(Connection.class);

	static {
		try (var input = Connection.class.getClassLoader().getResourceAsStream("application.properties")) {
			properties.load(input);
		} catch (IOException e) {
			log.error("Error reading properties", e);
		}
	}

	public static final String PLANETS_DOCUMENT = "planets";
	public static final String COMETS_DOCUMENT = "comets";

	private static MongoClient client;
	private static MongoDatabase database;

	public static MongoCollection<Document> connect(String document) {
		if (client == null) {
			client = MongoClients.create(getUrl());
			database = client.getDatabase(properties.getProperty(ConnectionConstants.DB_NAME_PROP_KEY));
		}

		return database.getCollection(document);
	}

	private static String getUrl() {
		var dbUrl = properties.getProperty(ConnectionConstants.DB_URL_PROP_KEY);
		var username = properties.getProperty(ConnectionConstants.DB_PASSWORD_PROP_KEY);
		var password = properties.getProperty(ConnectionConstants.DB_USERNAME_PROP_KEY);

		return dbUrl.replace(ConnectionConstants.USERNAME, username)
					.replace(ConnectionConstants.PASSWORD, password);
	}

	public static void disconect() {
		if (client != null)
			client.close();
	}

}
