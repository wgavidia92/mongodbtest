package com.mongodbtest.infrastructure;

import org.apache.log4j.Logger;

import com.mongodbtest.controller.PlanetsController;
import com.mongodbtest.infrastructure.service.ServiceFactory;

public final class ApplicationFactory {

	private ApplicationFactory() {}

	public static <T> Logger createLogger(Class<T> clazz) {
		return Logger.getLogger(clazz.getName());
	}

	public static PlanetsController controller() {
		return new PlanetsController(
				createLogger(PlanetsController.class),
				ServiceFactory.planetService(),
				ServiceFactory.cometService());
	}

	public static void endApp() {
		ServiceFactory.endApp();
	}

}
