package com.mongodbtest.repository;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.client.MongoCollection;

abstract sealed class Repository permits CometRepositoryImplementation, PlanetRepositoryImplementation {

	protected List<String> findData(MongoCollection<Document> dataSource, Bson filter) {
		var cursor = dataSource.find(filter).iterator();

		try {
			var collectionData = new ArrayList<String>();
			while (cursor.hasNext()) {
				collectionData.add(cursor.next().toJson());
			}
			return collectionData;
		} finally {
			cursor.close();
		}
	}

	protected List<String> insertData(MongoCollection<Document> dataSource, List<Document> documents) {
		var results = dataSource.insertMany(documents);

		return results.getInsertedIds()
				.values()
				.stream()
				.map(result -> result.asObjectId().getValue().toString())
				.toList();
	}

}
