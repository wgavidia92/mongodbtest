package com.mongodbtest.repository;

import java.util.List;

import org.bson.Document;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;

public final class PlanetRepositoryImplementation extends Repository implements PlanetRepository {

	private final MongoCollection<Document> dataSource;

	public PlanetRepositoryImplementation(MongoCollection<Document> dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public List<String> findAllPlanets() {
		return findData(dataSource, Filters.empty());
	}

	@Override
	public List<String> findPlanetsWithRings() {
		return findData(dataSource, Filters.eq("hasRings", true));
	}

}
