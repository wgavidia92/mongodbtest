package com.mongodbtest.repository;

import java.util.List;

import org.bson.Document;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import com.mongodbtest.dto.Comet;

public final class CometRepositoryImplementation extends Repository implements CometRepository {

	private final MongoCollection<Document> dataSource;

	public CometRepositoryImplementation(MongoCollection<Document> dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public List<String> insertComets(List<Comet> comets) {
		var documents = comets.stream()
				.map(this::generateDocument)
				.toList();

		return insertData(dataSource, documents);
	}

	private Document generateDocument(Comet comet) {
		return new Document("name", comet.name())
				.append("officialName", comet.officialName())
				.append("orbitalPeriod", comet.orbitalPeriod())
				.append("radius", comet.radius())
				.append("mass", comet.mass());
	}

	@Override
	public long updateRadius(double radius) {
		var filter = Filters.empty();
		var update = Updates.mul("radius", radius);
		var result = dataSource.updateMany(filter, update);
		return result.getModifiedCount();
	}

	@Override
	public long deleteWithOrbiltalPeriodBetween(double lowerOrbitalPeriod, double higherOrbitalPeriod) {
		var filter = Filters.and(
				Filters.gt("orbitalPeriod", lowerOrbitalPeriod),
				Filters.lt("orbitalPeriod", higherOrbitalPeriod));

		return dataSource.deleteMany(filter)
				.getDeletedCount();
	}

	@Override
	public long deleteAll() {
		var filter = Filters.empty();
		return dataSource.deleteMany(filter)
					.getDeletedCount();
	}
}
