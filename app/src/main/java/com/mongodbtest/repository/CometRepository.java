package com.mongodbtest.repository;

import java.util.List;

import com.mongodbtest.dto.Comet;

public interface CometRepository {

	List<String> insertComets(List<Comet> comets);

	long updateRadius(double radius);

	long deleteWithOrbiltalPeriodBetween(double lowerOrbitalPeriod, double higherOrbitalPeriod);

	long deleteAll();

}
