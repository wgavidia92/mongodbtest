package com.mongodbtest.repository;

import java.util.List;

public interface PlanetRepository {

	List<String> findAllPlanets();

	List<String> findPlanetsWithRings();

}
